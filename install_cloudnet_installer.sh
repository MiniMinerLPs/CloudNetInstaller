#!/bin/bash

INSTALLERVERSION="1.0.0"

function greenMessage {
  echo -e "\\033[32;1m${*}\033[0m"
}

function magentaMessage {
  echo -e "\\033[35;1m${*}\033[0m"
}

function cyanMessage {
  echo -e "\\033[36;1m${*}\033[0m"
}

function redMessage {
  echo -e "\\033[31;1m${*}\033[0m"
}

function yellowMessage {
  echo -e "\\033[33;1m${*}\033[0m"
}

function errorQuit {
  errorExit 'Exit now!'
}

function errorExit {
  redMessage "${@}"
  exit 1
}

function errorContinue {
  redMessage "Invalid option."
  return
}

function pre_initialization {

	if [ "$(id -u)" != "0" ]; then
	  cyanMessage "Change to root account required"
	  su root
	fi

	if [ "$(id -u)" != "0" ]; then
	  errorExit "Still not root, aborting"
	fi
	
	if [ -f /etc/centos-release ]; then
      yum -y -q install redhat-lsb
	fi
    
	if [ -f /etc/debian_version ]; then
	  apt-get install debconf-utils -y >/dev/null 2>&1
	  apt-get install lsb-release -y >/dev/null 2>&1
	fi
    
	MACHINE=$(uname -m)
	OS=$(lsb_release -i 2> /dev/null | grep 'Distributor' | awk '{print tolower($3)}')
	OSBRANCH=$(lsb_release -c 2> /dev/null | grep 'Codename' | awk '{print $2}')

	if [ -z "$OS" ]; then
	  errorExit "Error: Could not detect OS. Currently only Debian, Ubuntu and CentOS are supported. Aborting"!
	fi

	if [ -z "$OSBRANCH" ] && [ -f /etc/centos-release ]; then
	  errorExit "Error: Could not detect branch of OS. Aborting"
	fi

	if [ "$MACHINE" == "x86_64" ]; then
	  ARCH="amd64"
	else
	  errorExit "$MACHINE is not supported"!
	fi

	apt-get update >/dev/null 2>&1

	if [ "$OS" == "debian" ] ; then
	
	  echo "deb http://ftp.de.debian.org/debian "$branch"-backports main" > /etc/apt/sources.list >/dev/null 2>&1
	  apt-get install wget ca-certificates unzip gcc shc -y >/dev/null 2>&1
  
	elif [ "$OS" == "Ubuntu" ] ; then

	  apt-get install wget ca-certificates unzip shc -y >/dev/null 2>&1

	elif [ "$OS" == "CentOS" ] ; then

	  dhclient >/dev/null 2>&1
	  yum install wget ca-certificates unzip libc.so.6 csh -y >/dev/null 2>&1
	  cd /usr/local/src >/dev/null 2>&1
	  wget http://ftp.tu-chemnitz.de/pub/linux/dag/redhat/el6/en/x86_64/rpmforge/RPMS/shc-3.8.6-1.el6.rf.x86_64.rpm >/dev/null 2>&1
	  rpm -Uvh shc-3.8.6-1.el6.rf.x86_64.rpm >/dev/null 2>&1
	  rm shc-3.8.6-1.el6.rf.x86_64.rpm >/dev/null 2>&1

	fi

}

function checkOS {

	if [ -f /etc/debian_version ] || [ -f /etc/centos-release ]; then
	  return 0
	else
	  return 1
	fi

}

function initial {

  cyanMessage "Running pre initialization..."
  pre_initialization

	if [ checkOS ] ; then
	  greenMessage "OS supported. Installer goes on."
	else
	  redMessage "OS not supported. The installer only supports Debian, Ubuntu and CentOS. Aborting..."
	  exit 1
	fi

  cyanMessage "Installing nessesary dependencies... This may take up to 5 minutes..."

  install_dep

  cyanMessage "Downloading CloudNet installer..."

  rm /tmp/cloudnet_installer.sh >/dev/null 2>&1
  wget -q -O /tmp/cloudnet_installer.sh https://gitlab.com/MiniMinerLPs/CloudNetInstaller/raw/master/cloudnet_installer.sh.x >/dev/null 2>&1

  cyanMessage "Step 1 / 5 complete!"

  chmod 744 /tmp/cloudnet_installer.sh >/dev/null 2>&1

  cyanMessage "Step 2 / 5 complete!"

  echo ""
  greenMessage "Download successful! Installer will start in 5 seconds..."
  sleep 5
  bash /tmp/cloudnet_installer.sh

}

initial