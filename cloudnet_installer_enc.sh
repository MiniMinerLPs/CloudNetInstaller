#!/bin/bash

INSTALLERVERSION="1.0.0"

function greenMessage {
  echo -e "\\033[32;1m${*}\033[0m"
}

function magentaMessage {
  echo -e "\\033[35;1m${*}\033[0m"
}

function cyanMessage {
  echo -e "\\033[36;1m${*}\033[0m"
}

function redMessage {
  echo -e "\\033[31;1m${*}\033[0m"
}

function yellowMessage {
  echo -e "\\033[33;1m${*}\033[0m"
}

function errorQuit {
  errorExit 'Exit now!'
}

function errorExit {
  redMessage "${@}"
  exit 1
}

function errorContinue {
  redMessage "Invalid option."
  return
}

function pre_initialization {

	if [ "$(id -u)" != "0" ]; then
	  cyanMessage "Change to root account required"
	  su root
	fi

	if [ "$(id -u)" != "0" ]; then
	  errorExit "Still not root, aborting"
	fi
	
  echo "deb http://ftp.de.debian.org/debian wheezy-backports main" > /etc/apt/sources.list >/dev/null 2>&1
  apt-get update >/dev/null 2>&1
  apt-get -qq install wget ca-certificates unzip shc -y >/dev/null 2>&1

}

function start_proxy {

  service cloudnetproxy stop >/dev/null 2>&1
  service cloudnetproxy start >/dev/null 2>&1

	for i in {1..10} ; do
	  sleep 1
		if grep -m 1 "CNS" $CNPDIR/proxy.log >/dev/null 2>&1 ; then
		  greenMessage "Proxy started successfully!"
		  return 0
		else
		  redMessage "Error while starting CloudNet proxy! Log:"
		  cat $CNPDIR/proxy.log
		  echo ""
		  yellowMessage "Should the installer go on?"

			OPT1S=("Yes" "No" "Quit")
			select OPT1 in "${OPT1S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT1 == "Yes" ]]; then
			  return 0
			elif [[ $OPT1 == "No" ]] ; then
			  exit 1
			fi
		fi
	done
  return 0
}

function start_server {

  echo 1
  service cloudnetserver stop >/dev/null 2>&1
  sleep 3
  echo 2
	if grep -m 1 "Thanks" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "command" $CNSDIR/server.log >/dev/null 2>&1 ; then
	  echo 3
	else
	  echo 4
	  start_server
	fi
  echo 44
  service cloudnetserver start >/dev/null 2>&1
  echo 444

  sleep 5

	for i in {1..10} ; do
	  sleep 1
	  echo "test"
		if grep -m 1 "Thanks" $CNSDIR/server.log >/dev/null 2>&1 ; then
		  echo 5
		  redMessage "2Error while starting CloudNet server! Log:"
		  echo 6
		  cat $CNSDIR/server.log
		  yellowMessage "Retrying in 5 seconds..."
		  sleep 5
		  start_server
		  return 1
		elif grep -m 1 "language" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "BungeeCord" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "minecraft" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "name" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "How" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "joinpower" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "may" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "online" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "permanently" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "groupmode" $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "servertype" $CNSDIR/server.log >/dev/null 2>&1 ; then
		  redMessage "IMPORTANT:"
		  cyanMessage "CloudNet server instance started successfully but setup is not complete! Please answer the following questions:"
		  sleep 3
		  #service cloudnetserver stop ; sleep 1 ; service cloudnetserver start ; screen -R cloudnetserver | while true ; do sleep 1 ; if grep -m 1 "starting..." $CNSDIR/server.log >/dev/null 2>&1 ; then screen -S cloudnetserver -X stuff "stop^M" ; fi ; done
		  echo 8
		  
		    if grep -m 1 "language" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Please select a language [english, german]"

				function readcnssetup1 {
				  read -p "Language: " cnssetup1
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup1
					else
					  echo "....."
					fi
				}
			  readcnssetup1
			fi
			
			if grep -m 1 "servicekey" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Please enter the CNP service key"

				function readcnssetup2 {
				  read -p "Key: " cnssetup2
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup2
					else
					  echo "....."
					fi
				}
			  readcnssetup2
			fi
			
			if grep -m 1 "BungeeCord" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Chose one of these proxy-versions [\"Waterfall\", \"HexaCord\", \"Travertine\", \"Default (The normal BungeeCord version)\"]"

				function readcnssetup3 {
				  read -p "Proxy Version: " cnssetup3
					if [ ! grep -m 1 "not" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup3
					else
					  echo "....."
					fi
				}
			  readcnssetup3
			fi
			
			if grep -m 1 "minecraft" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Choose a minecraft server runnable [\"taco\", \"paper\", \"hose\", \"spigot\"]"

				function readcnssetup4 {
				  read -p "Server runnable: " cnssetup4
					if [ ! grep -m 1 "not" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup4
					else
					  echo "....."
					fi
				}
			  readcnssetup4
			fi
			
			if grep -m 1 "Taco" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Chose a minecraft-server version [\"1.8.8\", \"1.11.2\", \"1.12\"]"

				function readcnssetup5 {
				  read -p "Server version: " cnssetup5
					if [ ! grep -m 1 "not" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup5
					else
					  echo "....."
					fi
				}
			  readcnssetup5
			fi
			
			if grep -m 1 "Paper" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Chose a minecraft-server version [\"1.8.8\", \"1.11.2\", \"1.12\"]"

				function readcnssetup5 {
				  read -p "Server version: " cnssetup5
					if [ ! grep -m 1 "not" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup5
					else
					  echo "....."
					fi
				}
			  readcnssetup5
			fi
			
			if grep -m 1 "Hose" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Chose a minecraft-server version [\"1.8.8\", \"1.9.4\", \"1.10.2\", \"1.11.2\"]"

				function readcnssetup5 {
				  read -p "Server version: " cnssetup5
					if [ ! grep -m 1 "not" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup5
					else
					  echo "....."
					fi
				}
			  readcnssetup5
			fi
			
			if grep -m 1 "Spigot" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Chose a minecraft-server version [\"1.7.10\", \"1.8.8\", \"1.9.4\", \"1.10.2\", \"1.11.2\", \"1.12\"]"

				function readcnssetup5 {
				  read -p "Server version: " cnssetup5
					if [ ! grep -m 1 "not" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup5
					else
					  echo "....."
					fi
				}
			  readcnssetup5
			fi
			
			if grep -m 1 "name" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "What is the name of the group? (only characters A-Z; a-z; 0-9; \".\" and \"_\")"

				function readcnssetup6 {
				  read -p "Group name: " cnssetup6
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup6
					else
					  echo "....."
					fi
				}
			  readcnssetup6
			fi
			
			if grep -m 1 "How" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "How much memory should the servers have? (MB)"

				function readcnssetup7 {
				  read -p "Group name: " cnssetup7
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup7
					else
					  echo "....."
					fi
				}
			  readcnssetup7
			fi
			
			if grep -m 1 "joinpower" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "How much \"joinpower\" should the group need? (numeric)"

				function readcnssetup8 {
				  read -p "Joinpower: " cnssetup8
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup8
					else
					  echo "....."
					fi
				}
			  readcnssetup8
			fi
			
			if grep -m 1 "may" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "How many players may join? (numeric)"

				function readcnssetup9 {
				  read -p "Playercount: " cnssetup9
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup9
					else
					  echo "....."
					fi
				}
			  readcnssetup9
			fi
			
			if grep -m 1 "online" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "How many servers should be started when 100 players are online? (numeric)"

				function readcnssetup10 {
				  read -p "Servercount: " cnssetup10
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup10
					else
					  echo "....."
					fi
				}
			  readcnssetup10
			fi
			
			if grep -m 1 "permanently" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "How many servers should be permanently online? (numeric)"

				function readcnssetup11 {
				  read -p "Servercount: " cnssetup11
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup11
					else
					  echo "....."
					fi
				}
			  readcnssetup11
			fi
			
			if grep -m 1 "groupmode" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Which \"groupmode\" should the group have? [DYNAMIC, LOBBY, STATIC]"

				function readcnssetup12 {
				  read -p "Groupmode: " cnssetup12
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup12
					else
					  echo "....."
					fi
				}
			  readcnssetup12
			fi
			
			if grep -m 1 "servertype" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Which \"servertype\" should the group have? [BUKKIT, CLAUDRON]"

				function readcnssetup13 {
				  read -p "Servertype: " cnssetup13
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup13
					else
					  echo "....."
					fi
				}
			  readcnssetup13
			fi
			
			if grep -m 1 "ViaVersion" $CNSDIR/server.log >/dev/null 2>&1 ; then
			  yellowMessage "Do you want ViaVersion? (yes : no)"

				function readcnssetup14 {
				  read -p "Answer: " cnssetup14
					if [ ! grep -m 1 "Invalid" $CNSDIR/server.log >/dev/null 2>&1 ] ; then
					  redMessage "Invalid input. Error:"
					  cat
					  readcnssetup14
					else
					  echo "....."
					fi
				}
			  readcnssetup14
			fi

		  service cloudnetserver stop >/dev/null 2>&1
		  #java -XX:MaxGCPauseMillis=50 -Xmx128m -jar CloudNet-Server.jar --setup "cnssetup1,cnssetup2,cnssetup3,cnssetup4,cnssetup5,cnssetup6,cnssetup7,cnssetup8,cnssetup9,cnssetup10,cnssetup11"
		  sleep 2
		elif grep -m 1 "starting..." $CNSDIR/server.log >/dev/null 2>&1 || grep -m 1 "command" $CNSDIR/server.log >/dev/null 2>&1 ; then
		  echo 9
		  greenMessage "Server instance started successfully!"
		  return 0
		else
		  echo 10
		  redMessage "1Error while starting CloudNet server! Log:"
		  cat $CNSDIR/server.log

		  yellowMessage "Should the installer go on?"

			OPT1S=("Yes" "No" "Quit")
			select OPT1 in "${OPT1S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT1 == "Yes" ]]; then
			  return 0
			elif [[ $OPT1 == "No" ]] ; then
			  exit 1
			fi
		fi
	  return 0
	done
}

function install_proxy {

  redMessage "IMPORTANT: Please do not rename the jarfile. The installer will not work if the name isn't \"CloudNet-Proxy.jar\"!!"
  echo ""
  sleep 5

# Install

  yellowMessage "Where should the CloudNet proxy instance be installed?"

  read -p "Directory [/home/cloudnet/proxy]: " cnpdir

	if [ "$CNPDIR" == "" ] ; then
	  CNPDIR=/home/cloudnet/proxy
	fi

	if [ -d "$CNPDIR" ]; then

	  echo ""
	  redMessage "Not creating directory because it already exists."

		if [ -f "$CNPDIR/CloudNet-Proxy.jar" ] ; then

		  redMessage "CloudNet proxy instance found! Should the installer remove the current version? (No Userdata! Just the jarfile.)"

			OPT1S=("Yes" "No" "Quit")
			select OPT1 in "${OPT1S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT1 == "Yes" ]]; then
			  redMessage "Removing old CloudNet proxy instance..."
			  rm $CNPDIR/CloudNet-Proxy.jar
				if [ ! -f "$CNPDIR/CloudNet-Proxy.jar" ] ; then
				  greenMessage "Old CloudNet proxy instance removed!"
				else
				  redMessage "Cloudn't remove old CloudNet proxy instance. Please remove it manually."
				fi
			elif [[ $OPT1 == "No" ]] ; then
			  echo "" >/dev/null 2>&1
			fi
		fi
	else
	  mkdir -p "$CNPDIR" >/dev/null 2>&1
	fi

  echo ""
  cyanMessage "installing CloudNet proxy instance... This may take up to 5 minutes..."

  rm /tmp/CloudNet.zip >/dev/null 2>&1
  wget -q -O /tmp/CloudNet.zip http://download1139.mediafire.com/78m4kjq555kg/dy9jomjfbwztoxo/CloudNet-1.0-Pre.zip >/dev/null 2>&1

  cyanMessage "Step 1 / 5 complete!"

  rm -R /tmp/.cloudnetinstaller >/dev/null 2>&1
  mkdir -p /tmp/.cloudnetinstaller >/dev/null 2>&1

  cyanMessage "Step 2 / 5 complete!"

  cd /tmp >/dev/null 2>&1

  cyanMessage "Step 3 / 5 complete!"

  unzip CloudNet.zip -d ./.cloudnetinstaller >/dev/null 2>&1

  cyanMessage "Step 4 / 5 complete!"

  mv /tmp/.cloudnetinstaller/CloudNet-Proxy/CloudNet-Proxy.jar "$CNPDIR" >/dev/null 2>&1

  cyanMessage "Step 5 / 5 complete! Running cleanup..."

  rm -R /tmp/.cloudnetinstaller >/dev/null 2>&1
  rm /tmp/CloudNet.zip >/dev/null 2>&1

  echo ""
  greenMessage "Cleanup complete! CloudNet proxy instance installed successfully!"

# Autostart activation

  echo ""
  yellowMessage "Should the CloudNet proxy instance start at system boot?"

	OPT3S=("Yes" "No" "Quit")
	select OPT3 in "${OPT3S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT3 == "Yes" ]] ; then
	  rm /etc/init.d/cloudnetproxy >/dev/null 2>&1
	  echo "#! /bin/bash

### BEGIN INIT INFO
# Provides:             CNP
# Required-Start:       \$local_fs \$network
# Required-Stop:        \$local_fs \$network
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Description:          CloudNet cloudsystem proxy instance
### END INIT INFO

CNPDIR=$CNPDIR

function screen_exists {

if screen -list | grep -q \"cloudnetproxy\"; then
  return 0
else
  return 1
fi

}

function start {

  cd \$CNPDIR && screen -dmS cloudnetproxy bash -c \"java -XX:MaxGCPauseMillis=50 -XX:MaxPermSize=256M -Xmx128m -jar CloudNet-Proxy.jar > proxy.log > proxy.log\"

}

function stop {

  for session in \$(screen -ls | grep -o '[0-9]*\.cloudnetproxy'); do screen -S \"\${session}\" -X quit; done

}

function print_help {

  echo \"
Usage: {start | stop | restart | stauts  | help}

start   - start the CloudNet server instance
stop    - stop the CloudNet server instance
restart - restart the CloudNet server instance
status  - check if the CloudNet server instance is running
help    - show the help page
\" >&2
}

case \"\$1\" in

start)

if ! screen_exists ; then
  start
  echo \"CloudNet proxy instance started\"
else
  echo \"CloudNet proxy instance is already running\"
fi

;;

stop)

if screen_exists ; then
  stop
  echo \"CloudNet proxy instance stopped\"
else
  echo \"CloudNet proxy instance is already stopped\"
fi

;;

restart)

if screen_exists ; then
  stop
  echo \"CloudNet proxy instance stopped\"
  start
  echo \"CloudNet proxy instance started\"
else
  start
  echo \"CloudNet proxy instance started\"
fi

;;

status)

if screen_exists ; then
  echo \"CloudNet proxy instance is running\"
else
  echo \"CloudNet proxy instance is not running\"
fi

;;

help)

  print_help

;;

*)

  print_help

  exit 1

;;

esac" > /etc/init.d/cloudnetproxy

	  chmod 755 /etc/init.d/cloudnetproxy >/dev/null 2>&1
	  update-rc.d cloudnetproxy defaults >/dev/null 2>&1
	  echo ""
	  greenMessage "CloudNet proxy instance was added to system boot successfully!"
	elif [[ $OPT3 == "No" ]] ; then
	  echo ""
	  redMessage "Proxy instance will not start at system boot"
	fi

	if [ -f "$CNPDIR/config.json" ] ; then
	  cyanMessage "starting the CloudNet proxy for the first time..."
	  start_proxy
	  echo ""
	else
	  cyanMessage "starting the CloudNet proxy..."
	  start_proxy
	  echo ""
	fi
}

function install_server {

  redMessage "IMPORTANT: Please do not rename the jarfile. The installer will not work if the name isn't \"CloudNet-Server.jar\"!!"
  sleep 5

# Install

  echo ""
  yellowMessage "Where should the CloudNet server instance be installed?"

  read -p "Directory [/home/cloudnet/server]: " cnsdir

	if [ "$CNSDIR" == "" ] ; then
	  
	  CNSDIR=/home/cloudnet/server
	fi

	if [ -d "$CNSDIR" ]; then

	  echo ""
	  redMessage "Not creating directory because it already exists."

		if [ -f "$CNSDIR/CloudNet-Server.jar" ] ; then

		  redMessage "CloudNet server instance found! Should the installer remove the current version? (No Userdata! Just the jarfile.)"

			OPT1S=("Yes" "No" "Quit")
			select OPT1 in "${OPT1S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT1 == "Yes" ]]; then

			  redMessage "Removing old CloudNet server instance..."
			  rm $CNSDIR/CloudNet-Server.jar
				if [ ! -f "$CNSDIR/CloudNet-Server.jar" ] ; then
				  greenMessage "Old CloudNet server instance removed!"
				else
				  redMessage "Cloudn't remove old CloudNet server instance. Please remove it manually."
				fi
			elif [[ $OPT1 == "No" ]] ; then
			  echo "" >/dev/null 2>&1
			fi
		fi
	else
	  mkdir -p "$CNSDIR" >/dev/null 2>&1
	fi

  echo ""
  cyanMessage "installing CloudNet server instance... This may take up to 5 minutes..."

  rm /tmp/CloudNet.zip >/dev/null 2>&1
  wget -q -O /tmp/CloudNet.zip http://download1139.mediafire.com/78m4kjq555kg/dy9jomjfbwztoxo/CloudNet-1.0-Pre.zip >/dev/null 2>&1

  cyanMessage "Step 1 / 5 complete!"

  rm -R /tmp/.cloudnetinstaller >/dev/null 2>&1
  mkdir -p /tmp/.cloudnetinstaller >/dev/null 2>&1

  cyanMessage "Step 2 / 5 complete!"

  cd /tmp >/dev/null 2>&1

  cyanMessage "Step 3 / 5 complete!"

  unzip CloudNet.zip -d ./.cloudnetinstaller >/dev/null 2>&1

  cyanMessage "Step 4 / 5 complete!"

  mv /tmp/.cloudnetinstaller/CloudNet-Server/CloudNet-Server.jar "$CNSDIR" >/dev/null 2>&1

  cyanMessage "Step 5 / 5 complete! Running cleanup..."

  rm -R /tmp/.cloudnetinstaller >/dev/null 2>&1
  rm /tmp/CloudNet.zip >/dev/null 2>&1

  echo ""
  greenMessage "Cleanup complete! CloudNet server instance installed successfully!"

# Autostart activation

  echo ""
  yellowMessage "Should the CloudNet server instance start at system boot?"

	OPT3S=("Yes" "No" "Quit")
	select OPT3 in "${OPT3S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done
	
	if [[ $OPT3 == "Yes" ]] ; then
	  echo "" >/dev/null 2>&1
	  rm /etc/init.d/cloudnetserver
	  echo "#! /bin/bash

### BEGIN INIT INFO
# Provides:             CNS
# Required-Start:       \$local_fs \$network
# Required-Stop:        \$local_fs \$network
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Description:          CloudNet cloudsystem server instance
### END INIT INFO

CNSDIR=$CNSDIR

function screen_exists {

if screen -list | grep -q \"cloudnetserver\"; then
  return 0
else
  return 1
fi

}

function start {

  cd \$CNSDIR && screen -dmS cloudnetserver bash -c \"java -XX:MaxGCPauseMillis=50 -Xmx128m -jar CloudNet-Server.jar > server.log > server.log\"

}

function stop {

  for session in \$(screen -ls | grep -o '[0-9]*\.cloudnetserver'); do screen -S \"\${session}\" -X quit; done

}

function print_help {

  echo \"
Usage: {start | stop | restart | stauts  | help}

start   - start the CloudNet server instance
stop    - stop the CloudNet server instance
restart - restart the CloudNet server instance
status  - check if the CloudNet server instance is running
help    - show the help page
\" >&2
}

case \"\$1\" in

start)

if ! screen_exists ; then
  start
  echo \"CloudNet server instance started\"
else
  echo \"CloudNet server instance is already running\"
fi

;;

stop)

if screen_exists ; then
  stop
  echo \"CloudNet server instance stopped\"
else
  echo \"CloudNet server instance is already stopped\"
fi

;;

restart)

if screen_exists ; then
stop
  echo \"CloudNet server instance stopped\"
  start
  echo \"CloudNet server instance started\"
else
  start
  echo \"CloudNet server instance started\"
fi

;;

status)

if screen_exists ; then
  echo \"CloudNet server instance is running\"
else
  echo \"CloudNet server instance is not running\"
fi

;;

help)

  print_help

;;

*)

  print_help
  exit 1

;;

esac" > /etc/init.d/cloudnetserver

	  chmod 755 /etc/init.d/cloudnetserver  >/dev/null 2>&1
	  update-rc.d cloudnetserver defaults  >/dev/null 2>&1

	  echo ""
	  greenMessage "CloudNet server instance was added to system boot successfully!"

	elif [[ $OPT3 == "No" ]] ; then

	  echo ""
	  redMessage "Server instance will not start at system boot"

	fi

	if [ -f "$CNSDIR/config.json" ] ; then
	  cyanMessage "starting the CloudNet server for the first time..."
	  start_server
	  echo ""
	else
	  cyanMessage "starting the CloudNet server..."
	  start_server
	  echo ""
	fi

}

function remove_proxy_jarfile {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet proxy jarfile?"

	OPT7S=("No" "Yes" "Quit")
	select OPT7 in "${OPT7S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT7 == "No" ]]; then

	  remove_proxy_survey

	elif [[ $OPT7 == "Yes" ]]; then

	  rm $PROXYPATH/CloudNet-Proxy.jar

		if [ ! -a "$PROXYPATH/CloudNet-Proxy.jar" ] ; then
		
		  greenMessage "CloudNet proxy jarfile removed!"

		else

		  redMessage "Cloudn't remove CloudNet proxy jarfile. Please remove it manually."

		fi
	fi
}

function remove_proxy_userdata {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet proxy userdata? This includes EVERY file and EVERY folder except the jarfile."

	OPT8S=("No" "Yes" "Quit")
	select OPT8 in "${OPT8S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT8 == "No" ]]; then

	  remove_proxy_survey

	elif [[ $OPT8 == "Yes" ]]; then

	  cd $PROXYPATH && ls | grep -v CloudNet-Proxy.jar | xargs rm -r

	  FILESANDFOLDERS=`ls "$PROXYPATH" 2>/dev/null | wc -l`

		if [ "$FILESANDFOLDERS" == 1 ] || [ "$FILESANDFOLDERS" == 0 ] ; then

		  greenMessage "CloudNet proxy userdata removed!"

		else

		  redMessage "Cloudn't remove CloudNet proxy userdata. Please remove it manually."

		fi

	fi
}

function remove_proxy {

  echo ""
  yellowMessage "What should be removed?"

	OPT6S=("Jarfile" "Userdata" "Both" "Quit")
	select OPT6 in "${OPT6S[@]}" ; do
		case "$REPLY" in
		  1|2|3 ) break;;
		  4 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT6 == "Jarfile" ]]; then

	  remove_proxy_jarfile

	elif [[ $OPT6 == "Userdata" ]]; then

	  remove_proxy_userdata

	elif [[ $OPT6 == "Both" ]]; then

	  remove_proxy_jarfile
	  remove_proxy_userdata

	fi
}

function remove_proxy_survey {

  echo ""
  yellowMessage "please enter the CloudNet proxy instance path."

  read -p "Path [/home/cloudnet/proxy]:" proxypath

	if [ "$PROXYPATH" == "" ] ; then
	  PROXYPATH=/home/cloudnet/proxy
	fi

	if [ ! -d "$PROXYPATH" ] ; then

	  redMessage "Path doesn't exist. Pease enter a valid path."
	  remove_proxy_survey

	else

		if [ -d "$PROXYPATH/database" ] ; then

		  remove_proxy

		else

		  redMessage "CloudNet proxy instance installation not found."

		  echo ""
		  yellowMessage "Continue removing?"

			OPT5S=("Yes" "No" "Quit")
			select OPT5 in "${OPT5S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT5 == "Yes" ]]; then

			  remove_proxy

			elif [[ $OPT5 == "No" ]]; then

			  errorQuit

			fi

		fi

	fi
}

function remove_server_jarfile {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet server jarfile?"

	OPT7S=("No" "Yes" "Quit")
	select OPT7 in "${OPT7S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT7 == "No" ]]; then

	  remove_server_survey

	elif [[ $OPT7 == "Yes" ]]; then

	  rm $SERVERPATH/CloudNet-Server.jar

		if [ ! -a "$SERVERPATH/CloudNet-Server.jar" ] ; then

		  greenMessage "CloudNet server jarfile removed!"

		else

		  redMessage "Cloudn't remove CloudNet server jarfile. Please remove it manually."

		fi

	fi

}

function remove_server_userdata {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet server userdata? This includes EVERY file and EVERY folder except the jarfile."

	OPT8S=("No" "Yes" "Quit")
	select OPT8 in "${OPT8S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT8 == "No" ]]; then

	  remove_server_survey

	elif [[ $OPT8 == "Yes" ]]; then

	  cd $SERVERPATH && ls | grep -v CloudNet-Server.jar | xargs rm -r

	  FILESANDFOLDERS=`ls "$SERVERPATH" 2>/dev/null | wc -l`

		if [ "$FILESANDFOLDERS" == 1 ] || [ "$FILESANDFOLDERS" == 0 ] ; then

		  greenMessage "CloudNet server userdata removed!"

		else

		  redMessage "Cloudn't remove CloudNet server userdata. Please remove it manually."

		fi

	fi

}

function remove_server {

  echo ""
  yellowMessage "What should be removed?"

	OPT6S=("Jarfile" "Userdata" "Both" "Quit")
	select OPT6 in "${OPT6S[@]}" ; do
		case "$REPLY" in
		  1|2|3 ) break;;
		  4 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT6 == "Jarfile" ]]; then

	  remove_server_jarfile

	elif [[ $OPT6 == "Userdata" ]]; then

	  remove_server_userdata

	elif [[ $OPT6 == "Both" ]]; then

	  remove_server_jarfile
	  remove_server_userdata

	fi

}

function remove_server_survey {

  echo ""
  yellowMessage "please enter the CloudNet server instance path."

  read -p "Path [/home/cloudnet/server]:" serverpath

	if [ "$SERVERPATH" == "" ] ; then
	  SERVERPATH=/home/cloudnet/server
	fi

	if [ ! -d "$SERVERPATH" ] ; then

	  redMessage "Path doesn't exist. Pease enter a valid path."
	  remove_server_survey

	else

		if [ -d "$SERVERPATH/database" ] ; then

		  remove_server

		else

		  redMessage "CloudNet server instance installation not found."
		  echo ""
		  yellowMessage "Continue removing?"

			OPT5S=("Yes" "No" "Quit")
			select OPT5 in "${OPT5S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT5 == "Yes" ]]; then

			  remove_server

			elif [[ $OPT5 == "No" ]]; then

			  errorQuit

			fi

		fi

	fi
}

function initial {
  echo ""
  greenMessage "This is the installer for CloudNet 1.0.6. USE AT YOUR OWN RISK"!
  sleep 2
  cyanMessage "You can choose between installing and removeing CloudNet."
  sleep 2
  magentaMessage "Installer version: $INSTALLERVERSION"
  sleep 1
  redMessage "Script written by Arnim S. @ https://www.eu-hosting.ch"
  sleep 1

  pre_initialization

  echo ""
  yellowMessage "What should the script do?"

	OPT1S=("Install" "Remove" "Quit")
	select OPT1 in "${OPT1S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT1 == "Install" ]]; then

	  echo ""
	  yellowMessage "What should be installed?"

		OPT2S=("Proxy" "Server" "Both" "Quit")
		select OPT2 in "${OPT2S[@]}" ; do
			case "$REPLY" in
			  1|2|3 ) break;;
			  4 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT2 == "Proxy" ]] ; then

		  echo ""
		  cyanMessage "> CLOUDNET PROXY INSTANCE INSTALLATION <"
		  echo ""

		  install_proxy

		  echo ""
		  magentaMessage "Thank you for using this installer. For support, feature requests & bug reports, feel free to contact me on Twitter:"
		  magentaMessage "https://twitter.com/MiniMinerLPs :)"
		  echo ""

		elif [[ $OPT2 == "Server" ]] ; then

		  echo ""
		  cyanMessage "> CLOUDNET SERVER INSTANCE INSTALLATION <"
		  echo ""

		  install_server

		  echo ""
		  magentaMessage "Thank you for using this installer. For support, feature requests & bug reports, feel free to contact me on Twitter:"
		  magentaMessage "https://twitter.com/MiniMinerLPs :)"
		  echo ""

		elif [[ $OPT2 == "Both" ]] ; then

		  echo ""
		  cyanMessage "> CLOUDNET PROXY INSTANCE INSTALLATION <"
		  echo ""

		  install_proxy

		  echo ""
		  cyanMessage "> CLOUDNET SERVER INSTANCE INSTALLATION <"
		  echo ""

		  install_server

		  echo ""
		  magentaMessage "Thank you for using this installer. For support, feature requests & bug reports, feel free to contact me on Twitter:"
		  magentaMessage "https://twitter.com/MiniMinerLPs :)"
		  echo ""

		fi

	elif [[ $OPT1 == "Remove" ]]; then

	  echo ""
	  yellowMessage "What should be removed?"

		OPT5S=("Proxy" "Server" "Both" "Quit")
		select OPT5 in "${OPT5S[@]}" ; do
			case "$REPLY" in
			  1|2|3 ) break;;
			  4 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT5 == "Proxy" ]]; then

		  remove_proxy_survey

		elif [[ $OPT5 == "Server" ]]; then

		  remove_server_survey

		elif [[ $OPT5 == "Both" ]]; then

		  remove_proxy_survey
		  remove_server_survey

		fi

	  echo ""
	  magentaMessage "Thank you for using this installer. For support, feature requests & bug reports, feel free to contact me on Twitter:"
	  magentaMessage "https://twitter.com/MiniMinerLPs :)"
	  echo ""
	fi
}

initial